// This formats the day to mm/dd/yyy format

const lastDay =  require('./valid_last_day');

exports.getCurrentDateInCorrectFormat = (existingDate, period) => {
    if(existingDate === ""){
        var today = new Date();
    } else{
        var today = new Date(existingDate);
    }
    var dd = String(today.getDate()).padStart(2, '0');
    // period = 1 for monthly and 0 for yearly
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    if(period){
        var mm = String(today.getMonth() + 2).padStart(2, '0'); //January is 0!
    }else{
        var yyyy = today.getFullYear() + 1;
    }   
    var lastDd = lastDay.getLastValidDay(mm, yyyy, period);
    if(dd > lastDd){
      mm = (parseInt(mm) + 1).toString().padStart(2,0);
      dd = "01";
    }
    today = mm + '/' + dd + '/' + yyyy;
    return today;
}