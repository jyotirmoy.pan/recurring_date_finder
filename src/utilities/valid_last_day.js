// this returns the last valid day of the month and year. It considers leap year with 29 days in feb
exports.getLastValidDay = (mm,yyyy, period) => { 
  var testmm = parseInt(mm) - 1;
  var testyyyy = parseInt(yyyy);
  if(!period){
    testyyyy = testyyyy + 1;
  }
  switch(testmm){
    case 1:
      return (testyyyy % 4 == 0 && testyyyy % 100 == 0) || testyyyy % 400 === 0 ? "29":"28";
    case 4: case 6: case 8: case 10:
      return "30";
    case 0: case 3: case 5: case 7: case 11:
      return "31";
  }
}