# recurring date utility

Recurring date utility tool provides a date for your recurring subsciptions. 
It can provide a recurring date for monthly and annual users.

```js
var recurring = require('recurring_date_tool');

Use it in the below manner
recurring.getNextBillingDate(period, date)

The period here is the monthly or annual recurring period. 1 is for monthly and 0 is for annual

Date is the recurring billing date that is available. If current date or date is today then just pass empty string.

```

# Install

```console
$ npm i recurring_date_tool
```


# Contribute

If there is a new model you would like to support, or want to add a direct conversion between two existing models, please send us a pull request.

# License
Copyright &copy; 2019-*, Jyotirmoy Pan. Licensed under the [MIT License](LICENSE).
