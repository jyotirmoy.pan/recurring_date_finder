const currentDate = require('./src/utilities/format_Date');


// this is the core function that would call the recurring dates
exports.getNextBillingDate = (period, lastBilledDate) => {
    let finalBillingDate = "";
    // get lastBilledDate in mm/dd/yyyy format ex: 03/29/2019

    // if last billed date is not provided
    if((period && lastBilledDate != "") || 
        (!period && lastBilledDate != "")){ 
            finalBillingDate = currentDate.getCurrentDateInCorrectFormat(lastBilledDate, period);
    
    // if last billed date is provided
    }else if((period && lastBilledDate == "") || (!period && lastBilledDate == "")){
        finalBillingDate = currentDate.getCurrentDateInCorrectFormat("", period);
    } 
    return finalBillingDate;
}